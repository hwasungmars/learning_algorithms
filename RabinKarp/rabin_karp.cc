#include "rabin_karp.h"

int HashingBase94(const string& input) {
  string::const_iterator it;

  char zero_char = '!'; // The start of ASCII table with normal char.
  int hash = 0;
  int power_counter = 0;
  for(it=input.end()-1; it>input.begin()-1; it--) {
    hash += pow(94, power_counter)*(int(*it)-int(zero_char));
    power_counter++;
  }

  return hash;
}


bool IsStringContainedIn(string substring, string longstring) {
  return true;
}
