#include <iostream>
#include "rabin_karp.h"

using namespace std;

int main() {
  if (HashingBase94("!") == 1) {
    cout << "[PASS] HashingBase94 with ! returns 0 correctly!" << endl;
  }

  return 0;
}
