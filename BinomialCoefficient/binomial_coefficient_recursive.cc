#include <iostream>

using namespace std;

long BinomialCoefficient(int m, int n) {
  if (n==0) {
    return 1;
  } else {
    return BinomialCoefficient(m, n-1)*(m-n+1)/n;
  }
}

int main() {
  cout << BinomialCoefficient(300,1) << endl;
}

