#include <iostream>

using namespace std;

int BinomialCoefficientDynamicProgramming(int m, int n) {
  int bc_cache[m][n];

  for (int i=0; i<m; i++) {
    for (int j=0; j<n; j++) {
      bc_cache[i][j] = 0;
    }
  }

  for (int i=0; i<m; i++) {
    bc_cache[i][0] = 1;
  }
  for (int j=0; j<m; j++) {
    bc_cache[j][j] = 1;
  }

  for (int i=1; i<m; i++) {
    for (int j=0; j<n; j++) {
      bc_cache[i][j] = bc_cache[i-1][j] + bc_cache[i-1][j-1];
      cout << "bc_cache of " << i << j << bc_cache[i][j] << endl;
    }
  }

  return bc_cache[m-1][n-1];
}

int main() {
  cout << BinomialCoefficientDynamicProgramming(3, 2) << endl;

  return 0;
}
