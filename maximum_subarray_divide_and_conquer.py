# Maximum subarray algorithm using divide-and-conquer. The recursion relation
# for this algorithm is T(n) = 2T(n/2) + O(n), thus runtime is O(n log(n)).


def Conquer(A):
  """This function returns the maximum subarray of A in the middle."""
  half = len(A)/2

  # The starting array contains two elements that are the at the end of the
  # first half and the start of the second, i.e. A[half-1], and A[half]. In
  # Python notation this is A[half-1:half+1].

  # First scan through the first half.
  start = half-1
  i = half-2
  while i >= 0:
    if sum(A[i:half+1])>sum(A[start:half+1]):
      start = i
    i -= 1

  # Second scan through the second half.
  end = half+1
  i = half+2
  while i < len(A):
    if sum(A[half-1:i])>sum(A[half-1:end]):
      end = i
    i += 1

  return A[start:end]


def MaximumSubarrayDC(A):
  if len(A) == 1:
    return A
  else:
    half = len(A)/2
    first_candidate = MaximumSubarrayDC(A[:half])
    second_candidate = MaximumSubarrayDC(A[half:])
    middle_candidate = Conquer(A)
    sum_first_candidate = sum(first_candidate)
    sum_second_candidate = sum(second_candidate)
    sum_middle_candidate = sum(middle_candidate)

    if sum_first_candidate >= max(sum_second_candidate, sum_middle_candidate):
      return first_candidate
    elif sum_second_candidate >= max(sum_first_candidate, sum_middle_candidate):
      return second_candidate
    else:
      return middle_candidate


def testConquer():
  A = [1, 2, 3, 5 -4, 0, -3]
  assert A[0:4] == Conquer(A)
  print '[PASS] Conquer() has passed the test!'


def testMaximumSubarrayDC():
  A = [1, 2, 3, -1, -3, -5, -7, -9]
  assert A[0:3] == MaximumSubarrayDC(A)

  A = [1, 2, -1, 4, 5, 6, -1, -2]
  assert A[0:6] == MaximumSubarrayDC(A)
  print '[PASS] MaximumSubarrayDC() has passed the tests!'


testConquer()
testMaximumSubarrayDC()
